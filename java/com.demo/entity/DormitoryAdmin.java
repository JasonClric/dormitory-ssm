package com.southwind.entity;

import lombok.Data;

@Data
public class DormitoryAdmin {
//    表示宿舍管理员的唯一标识符
    private Integer id;
//    表示宿舍管理员的用户名，
    private String username;
//    表示宿舍管理员的密码
    private String password;
//    表示宿舍管理员的姓名
    private String name;
//    表示宿舍管理员的性别
    private String gender;
//    表示宿舍管理员的电话号码
    private String telephone;
}
