package com.southwind.service;

import com.southwind.entity.DormitoryAdmin;

import java.util.List;

public interface DormitoryAdminService {
//    `List<DormitoryAdmin> list()`: 此方法返回所有宿舍管理员的列表。
    public List<DormitoryAdmin> list();
//    `List<DormitoryAdmin> search(String key, String value)`: 此方法根据指定的键值对搜索宿舍管理员。`key`参数指定要搜索的属性名称，`value`参数指定属性值。
    public List<DormitoryAdmin> search(String key,String value);
//    `void save(DormitoryAdmin dormitoryAdmin)`: 此方法用于保存（添加）宿舍管理员。
    public void save(DormitoryAdmin dormitoryAdmin);
//    `void delete(Integer id)`: 此方法用于删除指定ID的宿舍管理员
    public void delete(Integer id);
//    `void update(DormitoryAdmin dormitoryAdmin)`: 此方法用于更新宿舍管理员的信息
    public void update(DormitoryAdmin dormitoryAdmin);
}
